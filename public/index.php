<?php
declare(strict_types = 1);
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Fraction Test</title>
    <script>window.MathJax = { MathML: { extensions: ["mml3.js", "content-mathml.js"]}};</script>
    <script type="text/javascript" async src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-MML-AM_CHTML"></script>
</head>
<body>
<h1>Mathematica – Strict typing stuff! <?= "\u{1F603}"; ?></h1>

<?php
/**
 * Created by PhpStorm.
 * User: akosma
 * Date: 05.08.17
 * Time: 21:59
 */

// Polyfill above (required by Chrome) courtesy of
// https://stackoverflow.com/a/29693359/133764

require_once('../vendor/autoload.php');

use training\akosma\math\Fraction;

$a = Fraction::create(1, 2);
$str = $a->getMathML();
echo "a: $str\n";
echo "<br/>";
echo  "LaTeX: " . $a->getLaTeX(true);
echo "<br/>";

$b = Fraction::create(2, 3);
$str = $b->getMathML();
echo "b: $str\n";
echo "<br/>";
echo  "TeX: " . $b->getTeX(true);
echo "<br/>";

$c = $a->add($b);
$str = $c->getMathML();
echo "c: a + b = $str";
echo "<br/>";
echo "AsciiMath: " . $c->getAsciiMath(true);
echo "<br/>";

$d = $c->subtract($b);
$str = $d->getMathML();
echo "d: c - b = $str\n";
echo "<br/>";

$equal = $d->equals($a) ? 'true' : 'false';
echo "d == a: $equal\n";
echo "<br/>";

$pi1 = Fraction::create(22, 7);
$pi2 = Fraction::create(355, 113);
$str = $pi1->getMathML();
echo "pi1: $str\n";
echo "<br/>";
$str = $pi2->getMathML();
echo "pi2: $str\n";
echo "<br/>";

$delta = abs($pi1() - $pi2());
echo "delta: $delta\n";
echo "<br/>";
?>
</body>

# Requires some libraries:
# brew install phpunit
# brew install phpdocumentor
# brew install deployer

all: test

clean:
	rm -rf _build

docs:
	phpdoc

test:
	phpunit

coverage:
	phpunit --testdox --coverage-text

html:
	phpunit --coverage-html _build/coverage --testdox-html _build/testdox.html --coverage-text

install-dev:
	php composer.phar install

install: clean
	php composer.phar install --no-ansi --no-dev --no-interaction --no-progress --no-scripts --optimize-autoloader

deploy:
	dep deploy --hosts akosma.org

rollback:
	dep rollback --hosts akosma.org

remote-test:
	dep test --hosts akosma.org


<?php
declare(strict_types = 1);

/**
 * Created by PhpStorm.
 * User: akosma
 * Date: 05.08.17
 * Time: 12:03
 */

namespace training\akosma\tests;

use PHPUnit\Framework\TestCase;
use training\akosma\math\Fraction;

/**
 * Class FractionCalculationTest
 *
 * Provides some advanced use cases of the Fraction class to calculate Pi.
 *
 * Wikipedia links taken from
 * https://en.wikipedia.org/wiki/List_of_formulae_involving_%CF%80
 *
 * @package training\akosma\math
 */
class FractionCalculationTest extends TestCase {
    const PI = 3.141592653589;

    /**
     * Tries to reach a suitable value for Pi using the
     * Wallis Product.
     *
     * Adapted from
     * https://en.wikipedia.org/wiki/Wallis_product
     */
    function testWallisProductTendsToPiDividedBy2() {
        /** @var Fraction[] */
        $fractions = [Fraction::create(2, 1)];
        for ($n = 1; $n < 16; $n++) {
            $twoN = 2 * $n;
            $f1 = Fraction::create($twoN, $twoN - 1);
            $f2 = Fraction::create($twoN, $twoN + 1);
            $fractions[] = $f1->multiply($f2);
        }

        $result = array_reduce($fractions,
            'training\akosma\math\Fraction::product', Fraction::one());
        $this->assertTrue($this->differenceFromPi($result) < 0.06);
    }

    /**
     * Tries to reach a suitable value for Pi using the
     * Bailey-Borwein-Plouffe Formula.
     *
     * Adapted from
     * https://en.wikipedia.org/wiki/Bailey%E2%80%93Borwein%E2%80%93Plouffe_formula
     */
    public function testBaileyBorweinPlouffeFormulaTendsToPi() {
        $result = Fraction::zero();

        for ($k = 0; $k < 4; $k++) {
            $sixteenK = (integer) pow(16, $k);
            $factor = Fraction::create(1, $sixteenK);

            $first = Fraction::create(4, 8 * $k + 1);
            $second = Fraction::create(2, 8 * $k + 4);
            $third = Fraction::create(1, 8 * $k + 5);
            $fourth = Fraction::create(1, 8 * $k + 6);
            $stuff =
                $first->subtract($second)->subtract($third)->subtract($fourth);

            $fraction = $factor->multiply($stuff);
            $result = $result->add($fraction);
        }

        $this->assertTrue($this->differenceFromPi($result) < 0.0000002);
    }

    /**
     * Calculates the difference from Pi from the Fraction passed as parameter.
     *
     * @param $result
     *
     * @return float
     */
    private function differenceFromPi(Fraction $result): float {
        return (float) abs($result->getValue() - self::PI);
    }
}

<?php
declare(strict_types = 1);

/**
 * Created by PhpStorm.
 * User: akosma
 * Date: 06.08.17
 * Time: 16:58
 */

namespace training\akosma\tests;

use PHPUnit\Framework\TestCase;
use training\akosma\math\Functions;

class FunctionsTest extends TestCase {
    function testGreatestCommonDivisor() {
        $divisor = Functions::gcd(54, 24);
        $this->assertEquals(6, $divisor);

        // Interesting cases to step into and watch the recursion
        $again = Functions::gcd(84198778321, 97821321);
        $this->assertEquals(29, $again);

        // These two are primes to each other
        $finally = Functions::gcd(841987, 978213);
        $this->assertEquals(1, $finally);
    }
}

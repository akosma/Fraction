<?php
declare(strict_types = 1);

/**
 * Created by PhpStorm.
 * User: akosma
 * Date: 05.08.17
 * Time: 12:51
 */

namespace training\akosma\tests;

use PHPUnit\Framework\TestCase;
use training\akosma\math\Fraction;

class FractionCreationTest extends TestCase {
    public function testFractionsCanBeCreatedFromIntegers() {
        $f = Fraction::create(1, 2);
        $str = (string)$f;
        $this->assertEquals("1 / 2", $str);
    }

    public function testFractionsAreCached() {
        $f1 = Fraction::create(345, 456);
        $f2 = Fraction::createFromString("345/ 456");
        $this->assertEquals($f1, $f2);
        $this->assertTrue($f1 === $f2);
    }

    public function testFractionsCanBeCreatedFromString() {
        $f1 = Fraction::create(1, 2);
        $f2 = Fraction::createFromString("1/2");
        $f3 = Fraction::createFromString("1 / 2");
        $f4 = Fraction::createFromFloat(0.5);
        $this->assertEquals($f1, $f2);
        $this->assertEquals($f1, $f3);
        $this->assertEquals($f1, $f4);
    }

    public function testFractionsCanNotBeCreatedFromInvalidString() {
        try {
            Fraction::createFromString("15343456:23435");
            $this->assertTrue(false);
        }
        catch (\Exception $exception) {
            $this->assertTrue(true);
        }
    }

    public function testFractionsHasZero() {
        $zero1 = Fraction::zero();
        $zero2 = Fraction::zero();
        $this->assertEquals($zero1, $zero2);
        $this->assertTrue($zero1 === $zero2);
    }

    public function testFractionsHasOne() {
        $oneA = Fraction::one();
        $oneB = Fraction::one();
        $this->assertEquals($oneA, $oneB);
        $this->assertTrue($oneA === $oneB);
    }

    public function testFractionsCanBeCreatedFromFloat() {
        $f1 = Fraction::create(1, 2);
        $f2 = Fraction::createFromFloat(0.5);
        $this->assertTrue($f1->equals($f2));

        $f3 = Fraction::create(1, 3);
        $f4 = Fraction::createFromFloat(0.333333);
        $this->assertTrue($f3->similar($f4));

        $f5 = Fraction::createFromFloat(0.048);
        $f6 = Fraction::createFromString("90 / 1875");
        $f7 = Fraction::create(6, 125); // Reduced form
        $this->assertTrue($f6->equals($f5));
        $this->assertTrue($f7->equals($f5));
    }

    public function testFractionsCanBeCreatedFromInt() {
        $f1 = Fraction::createFromInt(345);
        $f2 = Fraction::create(345, 1);
        $this->assertTrue($f1->equals($f2));
    }

    public function testCrashesIfDenominatorIsZero() {
        try {
            Fraction::create(1, 0);
            $this->assertTrue(false);
        }
        catch (\Exception $exception) {
            $this->assertTrue(true);
        }
    }

    public function testFractionsAreReducedAtConstruction() {
        $f1 = Fraction::create(1, 2);
        $f2 = Fraction::create(2, 4);

        $this->assertTrue($f1->equals($f2));
    }

    public function testFractionsReturnMathmlRepresentation() {
        $f = Fraction::create(1, 2);
        $str = $f->getMathML();
        $expected = <<<MATHML
<math xmlns="http://www.w3.org/1998/Math/MathML">
<mfrac>
   <mn>1</mn>
   <mi>2</mi>
</mfrac>
</math>
MATHML;

        $this->assertEquals($expected, $str);
    }

    public function testFractionsReturnLatexRepresentation() {
        $f = Fraction::create(1, 2);
        $str = $f->getLaTeX();
        $expected = '\frac{1}{2}';
        $this->assertEquals($expected, $str);

        $str = $f->getLaTeX(true);
        $expected = '$$\frac{1}{2}$$';
        $this->assertEquals($expected, $str);
    }

    public function testFractionsReturnTexRepresentation() {
        $f = Fraction::create(1, 2);
        $str = $f->getTeX();
        $expected = '$1 \over 2$';
        $this->assertEquals($expected, $str);

        $str = $f->getTeX(true);
        $expected = '$$1 \over 2$$';
        $this->assertEquals($expected, $str);
    }

    public function testFractionsReturnAsciimathRepresentation() {
        $f = Fraction::create(1, 2);
        $str = $f->getAsciiMath();
        $expected = '1/2';
        $this->assertEquals($expected, $str);

        $str = $f->getAsciiMath(true);
        $expected = '`1/2`';
        $this->assertEquals($expected, $str);
    }
}

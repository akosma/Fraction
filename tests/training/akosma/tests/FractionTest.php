<?php
declare(strict_types = 1);

/**
 * Created by PhpStorm.
 * User: akosma
 * Date: 04.08.17
 * Time: 10:55
 */

namespace training\akosma\tests;

use PHPUnit\Framework\TestCase;
use training\akosma\math\Fraction;

class FractionTest extends TestCase {
    public function testGettersWork() {
        $f = Fraction::create(1, 2);
        $n = $f->getNumerator();
        $d = $f->getDenominator();
        $this->assertEquals(1, $n);
        $this->assertEquals(2, $d);
    }

    public function testFractionsCanBeCopied() {
        $f = Fraction::create(1, 2);
        $c = clone $f;
        $this->assertTrue($f->equals($c));
        $this->assertTrue($f !== $c);
    }

    public function testFractionReturnsFloatValue() {
        $f = Fraction::create(1, 2);
        $n = $f->getNumerator();
        $d = $f->getDenominator();
        $v1 = $f->getValue();

        $this->assertEquals(1, $n);
        $this->assertEquals(2, $d);
        $this->assertEquals(0.5, $v1);

        // You can also use this syntax to get the fraction value
        $v2 = $f();
        $this->assertEquals($v1, $v2);
    }

    function testCanBeCompared() {
        $f1 = Fraction::create(10, 100);
        $f2 = Fraction::create(1, 10);
        $this->assertTrue($f1->equals($f2));
        $this->assertTrue($f1->equals($f1));
        $this->assertTrue($f2->equals($f2));

        $f3 = Fraction::create(24, 63);
        $f4 = Fraction::create(2454, 43234);
        $this->assertFalse($f3->equals($f4));
        $this->assertFalse($f4->equals($f3));
        $this->assertTrue($f3->greaterThan($f4));
        $this->assertTrue($f4->lessThan($f3));

        $pi1 = Fraction::create(22, 7);
        $pi2 = Fraction::create(355, 113);
        $this->assertTrue($pi1->similar($pi2));
    }

    function testFractionsAreEqualToThemselves() {
        $f = Fraction::create(1, 2);
        $this->assertTrue($f->equals($f));
    }

    function testFractionsCanBeSimilarToAnother() {
        $pi1 = Fraction::create(22, 7);
        $pi2 = Fraction::create(355, 113);
        $this->assertTrue($pi1->similar($pi2));
    }

    function testFractionsCanBeInverted() {
        $f1 = Fraction::create(1, 2);
        $this->assertEquals(1, $f1->getNumerator());
        $this->assertEquals(2, $f1->getDenominator());
        $f2 = $f1->invert();
        $this->assertEquals(2, $f2->getNumerator());
        $this->assertEquals(1, $f2->getDenominator());
    }

    function testFractionsCanBeNegated() {
        $f1 = Fraction::create(1, 2);
        $f2 = $f1->negate();
        $this->assertLessThan(0, $f2->getNumerator());
        $f3 = $f2->negate();
        $this->assertGreaterThan(0, $f3->getNumerator());
    }
}

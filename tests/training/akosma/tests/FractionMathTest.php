<?php
declare(strict_types = 1);

/**
 * Created by PhpStorm.
 * User: akosma
 * Date: 05.08.17
 * Time: 12:51
 */

namespace training\akosma\tests;

use PHPUnit\Framework\TestCase;
use training\akosma\math\Fraction;

class FractionMathTest extends TestCase {
    public function testFractionsCanBeAdded() {
        $f1 = Fraction::create(1, 2);
        $f2 = Fraction::create(2, 3);
        $f3 = $f1->add($f2);
        $this->assertEquals(7, $f3->getNumerator());
        $this->assertEquals(6, $f3->getDenominator());

        $f4 = Fraction::sum($f1, $f2);
        $this->assertTrue($f4->equals($f3));

        $f4 = Fraction::create(1, 2);
        $f5 = Fraction::create(1, 4);
        $f6 = Fraction::create(1, 68);
        $f7 = $f4->add($f5)->add($f6);
        $this->assertEquals(13, $f7->getNumerator());
        $this->assertEquals(17, $f7->getDenominator());
    }

    function testZeroIsNeutralInAddition() {
        $zero = Fraction::zero();
        $f2 = Fraction::create(1, 2);
        $f3 = $zero->add($f2);
        $this->assertTrue($f2->equals($f3));
    }

    public function testAdditionIsCommutative() {
        $f1 = Fraction::create(1, 2);
        $f2 = Fraction::create(2, 3);
        $f3 = $f1->add($f2);
        $f4 = $f2->add($f1);
        $this->assertTrue($f4->equals($f3));
    }

    function testFractionsCanBeMultiplied() {
        $f1 = Fraction::create(-8, 21);
        $f2 = Fraction::create(2, 43);
        $f3 = $f1->multiply($f2);
        $this->assertEquals(-16, $f3->getNumerator());
        $this->assertEquals(903, $f3->getDenominator());

        $f3bis = Fraction::product($f1, $f2);
        $this->assertTrue($f3bis->equals($f3));

        $f4 = Fraction::create(2, 3);
        $f5 = Fraction::create(3, 4);
        $f6 = $f4->multiply($f5);
        $this->assertEquals(1, $f6->getNumerator());
        $this->assertEquals(2, $f6->getDenominator());
    }

    function testOneIsNeutralInMultiplication() {
        $f1 = Fraction::one();
        $f2 = Fraction::create(1, 2);
        $f3 = $f1->multiply($f2);
        $this->assertTrue($f2->equals($f3));
    }

    public function testProductIsCommutative() {
        $f1 = Fraction::create(1, 2);
        $f2 = Fraction::create(2, 3);
        $f3 = $f1->multiply($f2);
        $f4 = $f2->multiply($f1);
        $this->assertTrue($f4->equals($f3));
    }

    function testFractionsCanBeSubtracted() {
        $f1 = Fraction::create(-24, 63);
        $f2 = Fraction::create(2, 43);
        $f3 = $f1->subtract($f2);
        $this->assertEquals(-386, $f3->getNumerator());
        $this->assertEquals(903, $f3->getDenominator());
    }

    function testFractionsCanBeDivided() {
        $f1 = Fraction::create(-16, 903);
        $f2 = Fraction::create(2, 43);
        $f3 = $f1->divide($f2);
        $this->assertEquals(-8, $f3->getNumerator());
        $this->assertEquals(21, $f3->getDenominator());
    }

    public function testFractionCanReturnSquareRoot() {
        $f1 = Fraction::create(1, 16);
        $f2 = Fraction::create(1, 4);
        $result = $f1->sqrt();
        $this->assertEquals($f2->getValue(), $result);
    }

    public function testFractionCanBeElevatedToPower() {
        $f1 = Fraction::create(1, 16);
        $f2 = Fraction::create(1, 4);
        $result = $f1->pow(0.5);
        $this->assertEquals($f2, $result);

        $f3 = Fraction::create(3, 5);
        $f4 = Fraction::create(27, 125);
        $result = $f3->pow(3);
        $this->assertEquals($f4, $result);
    }
}

<?php
declare(strict_types = 1);

/**
 * Created by PhpStorm.
 * User: akosma
 * Date: 04.08.17
 * Time: 10:55
 */

namespace training\akosma\math;

/**
 * Class Fraction
 *
 * Represents a fractional number with integer numerator and denominator.
 * Fraction objects are immutable; to modify an instance you should create
 * a new one instead. Most common mathematical operations are available.
 *
 * @package training\akosma\math
 */
final class Fraction {
    /**
     * The numerator of the current fraction.
     *
     * @var int
     */
    private $numerator;
    /**
     * The denominator of the current fraction.
     *
     * @var int
     */
    private $denominator;

    /**
     * Fraction constructor.
     * Do not use it; use the ::create static functions instead.
     *
     * @param int $num
     * @param int $den
     */
    private function __construct(int $num, int $den) {
        if ($den === 0) {
            throw new \InvalidArgumentException("The denominator must be greater than zero");
        };
        $absnum = (int) abs($num);
        $gcd = Functions::gcd($absnum, $den);
        $this->numerator = $num / $gcd;
        $this->denominator = $den / $gcd;
    }

    /**
     * Creates a Fraction::create instance using two integers.
     *
     * Created instances are kept in a cache, so that when a user requests
     * a "1/2" it will always get the same instance. This works because
     * Fraction instances are immutable, and they cannot be changed, only
     * combined, and helps reducing the memory consumption for applications
     * with a large number of Fraction instances at any given time in memory.
     *
     * @param int $num
     * @param int $den
     *
     * @return \training\akosma\math\Fraction
     */
    public static function create(int $num, int $den): Fraction {
        static $cache = [];
        $key = "$num / $den";
        if (array_key_exists($key, $cache)) {
            return $cache[$key];
        }
        $fraction = new self($num, $den);
        $cache[$key] = $fraction;
        return $fraction;
    }

    /**
     * Creates a Fraction instance using a string representation similar
     * to "1 / 2". This function expects a "/" sign as the separator of the
     * numerator and denominator, and will trim all whitespace as required.
     *
     * @param string $representation
     *
     * @return \training\akosma\math\Fraction
     */
    public static function createFromString(string $representation): Fraction {
        $parts = explode("/", $representation);
        if (count($parts) < 2) {
            throw new \InvalidArgumentException("Invalid string representation");
        }
        $parts = array_map("trim", $parts);
        list($n, $d) = $parts;

        return self::create((int) $n, (int) $d);
    }

    /**
     * Creates a Fraction object using the floating point number passed as
     * parameter. For periodic floating point numbers such as 1/3, this
     * conversion cannot be made exactly, and a similar – albeit different –
     * fraction might be returned instead.
     *
     * @param float $number
     *
     * @return \training\akosma\math\Fraction
     */
    public static function createFromFloat(float $number): Fraction {
        $str = strval($number);
        $len = strlen($str);
        $den = (int) pow(10, $len);
        $num = $number * $den;

        return self::create((int) $num, $den);
    }

    /**
     * Creates a Fraction object from an integer value, which returns
     * a Fraction with denominator 1.
     *
     * @param int $number
     *
     * @return \training\akosma\math\Fraction
     */
    public static function createFromInt(int $number): Fraction {
        return self::create($number, 1);
    }

    /**
     * Returns a fraction with numerator 0 (zero) and denominator 1 (one)
     *
     * @return \training\akosma\math\Fraction
     */
    public static function zero(): Fraction {
        return self::createFromInt(0);
    }

    /**
     * Returns a fraction with numerator 1 (one) and denominator 1 (one)
     *
     * @return \training\akosma\math\Fraction
     */
    public static function one(): Fraction {
        return self::createFromInt(1);
    }

    /**
     * Returns the sum of two fractions.
     *
     * @param \training\akosma\math\Fraction $f1
     * @param \training\akosma\math\Fraction $f2
     *
     * @return \training\akosma\math\Fraction
     */
    public static function sum(Fraction $f1, Fraction $f2): Fraction {
        return $f1->add($f2);
    }

    /**
     * Returns the product of two fractions.
     *
     * @param \training\akosma\math\Fraction $f1
     * @param \training\akosma\math\Fraction $f2
     *
     * @return \training\akosma\math\Fraction
     */
    public static function product(Fraction $f1, Fraction $f2): Fraction {
        return $f1->multiply($f2);
    }

    /**
     * Getter for the numerator of the current instance.
     *
     * @return int
     */
    public function getNumerator(): int {
        return $this->numerator;
    }

    /**
     * Getter for the denominator of the current instance.
     *
     * @return int
     */
    public function getDenominator(): int {
        return $this->denominator;
    }

    /**
     * Returns the floating point number represented by this fraction.
     *
     * @return float
     */
    public function getValue(): float {
        return $this->getNumerator() / $this->getDenominator();
    }

    /**
     * Returns a boolean specifying whether the current fraction is equal
     * to the one passed as parameter. From the PHP documentation:
     *
     * "When using the comparison operator (==), object variables are compared
     * in a simple manner, namely: Two object instances are equal if they have
     * the same attributes and values (values are compared with ==), and are
     * instances of the same class."
     *
     * @param \training\akosma\math\Fraction $fraction
     *
     * @return bool
     */
    public function equals(Fraction $fraction): bool {
        /** @noinspection PhpNonStrictObjectEqualityInspection */
        return ($this == $fraction);
    }

    /**
     * Specifies whether the current fraction is greater than the one
     * passed as parameter.
     *
     * @param \training\akosma\math\Fraction $fraction
     *
     * @return bool
     */
    public function greaterThan(Fraction $fraction): bool {
        $v1 = $this->getValue();
        $v2 = $fraction->getValue();
        $result = $v1 > $v2;

        return $result;
    }

    /**
     * Specifies whether the current fraction is smaller than the
     * one passed as parameter.
     *
     * @param \training\akosma\math\Fraction $fraction
     *
     * @return bool
     */
    public function lessThan(Fraction $fraction): bool {
        $greater = $this->greaterThan($fraction);
        $result = ! $greater;

        return $result;
    }

    /**
     * Specifies whether the current fraction is similar (within
     * a specified delta) to the one passed as parameter. The default
     * delta value is 0.01.
     *
     * @param \training\akosma\math\Fraction $fraction
     * @param float                          $delta
     *
     * @return bool
     */
    public function similar(Fraction $fraction, $delta = 0.01): bool {
        $v1 = $this->getValue();
        $v2 = $fraction->getValue();
        $result = abs($v1 - $v2) < $delta;

        return $result;
    }

    /**
     * Inverts this fraction.
     *
     * @return \training\akosma\math\Fraction
     */
    public function invert(): Fraction {
        list($n, $d) = $this->toArray();
        $result = Fraction::create($d, $n);

        return $result;
    }

    /**
     * Returns the negative of this fraction.
     *
     * @return \training\akosma\math\Fraction
     */
    public function negate(): Fraction {
        list($n, $d) = $this->toArray();
        $result = Fraction::create(-1 * $n, $d);

        return $result;
    }

    /**
     * Adds the current Fraction with the one passed as parameter.
     *
     * @param \training\akosma\math\Fraction $fraction
     *
     * @return \training\akosma\math\Fraction
     */
    public function add(Fraction $fraction): Fraction {
        list($n1, $d1) = $this->toArray();
        list($n2, $d2) = $fraction->toArray();
        $rn = $n1 * $d2 + $n2 * $d1;
        $rd = $d1 * $d2;
        $result = Fraction::create($rn, $rd);

        return $result;
    }

    /**
     * Multiplies the current Fraction with the one passed as parameter.
     *
     * @param \training\akosma\math\Fraction $fraction
     *
     * @return \training\akosma\math\Fraction
     */
    public function multiply(Fraction $fraction): Fraction {
        list($n1, $d1) = $this->toArray();
        list($n2, $d2) = $fraction->toArray();
        $result = Fraction::create($n1 * $n2, $d1 * $d2);

        return $result;
    }

    /**
     * Subtracts the current fraction with the one passed as parameter.
     *
     * @param \training\akosma\math\Fraction $fraction
     *
     * @return \training\akosma\math\Fraction
     */
    public function subtract(Fraction $fraction): Fraction {
        $negated = $fraction->negate();
        $result = $this->add($negated);

        return $result;
    }

    /**
     * Divides the current fraction with the one passed as parameter.
     *
     * @param \training\akosma\math\Fraction $fraction
     *
     * @return \training\akosma\math\Fraction
     */
    public function divide(Fraction $fraction): Fraction {
        $inverted = $fraction->invert();
        $result = $this->multiply($inverted);

        return $result;
    }

    /**
     * Returns the floating point value of the square root of the
     * current instance.
     *
     * @return float
     */
    public function sqrt(): float {
        $value = $this->getValue();

        return sqrt($value);
    }

    /**
     * Returns a Fraction::create with the current instance elevated
     * to the power specified as parameter.
     *
     * @param float $exp
     *
     * @return \training\akosma\math\Fraction
     */
    public function pow(float $exp): Fraction {
        list($n, $d) = $this->toArray();
        $nn = (int) pow($n, $exp);
        $dd = (int) pow($d, $exp);

        return Fraction::create($nn, $dd);
    }

    /**
     * Returns the MathML representation of the current fraction.
     *
     * @return string
     */
    public function getMathML(): string {
        list($num, $den) = $this->toArray();
        $str = <<<MATHML
<math xmlns="http://www.w3.org/1998/Math/MathML">
<mfrac>
   <mn>$num</mn>
   <mi>$den</mi>
</mfrac>
</math>
MATHML;

        return $str;
    }

    /**
     * Returns the LaTeX representation of the current fraction.
     *
     * The parameter specifies that the output will be compatible with
     * browsers using the MathJax library https://www.mathjax.org/
     *
     * @param bool $mathJaxCompatible
     *
     * @return string
     */
    public function getLaTeX(bool $mathJaxCompatible = false): string {
        list($num, $den) = $this->toArray();
        $delimiter = ($mathJaxCompatible) ? "$$" : "";

        return $delimiter . '\frac{' . $num . '}{' . $den . '}' . $delimiter;
    }

    /**
     * Returns the TeX representation of the current fraction.
     *
     * The parameter specifies that the output will be compatible with
     * browsers using the MathJax library https://www.mathjax.org/
     *
     * @param bool $mathJaxCompatible
     *
     * @return string
     */
    public function getTeX(bool $mathJaxCompatible = false): string {
        list($num, $den) = $this->toArray();
        $delimiter = ($mathJaxCompatible) ? "$$" : "$";

        return $delimiter . $num . ' \over ' . $den . $delimiter;
    }

    /**
     * Returns the AsciiMath representation of the current fraction.
     *
     * For more information check http://asciimath.org/
     *
     * The parameter specifies that the output will be compatible with
     * browsers using the MathJax library https://www.mathjax.org/
     *
     * @param bool $mathJaxCompatible
     *
     * @return string
     */
    public function getAsciiMath(bool $mathJaxCompatible = false): string {
        list($num, $den) = $this->toArray();
        $delimiter = ($mathJaxCompatible) ? "`" : "";

        return $delimiter . $num . '/' . $den . $delimiter;
    }

    /**
     * Makes the current instance callable, and returns the float value.
     *
     * @return float
     */
    public function __invoke() {
        return $this->getValue();
    }

    /**
     * Returns a string representation of the current instance.
     *
     * @return string
     */
    public function __toString(): string {
        list($n, $d) = $this->toArray();

        return "$n / $d";
    }

    /**
     * Returns an array of two values; the first value in the array
     * is the numerator, the second is the denominator. This method
     * simplifies the code in calculations that require both elements
     * in separate variables.
     *
     * @return int[]
     */
    public function toArray(): array {
        $result = [$this->getNumerator(), $this->getDenominator()];

        return $result;
    }

    /**
     * Creates a copy of the current instance. This method
     * does exactly the same as the default implementation, and
     * is only here for demonstration purposes.
     *
     * @return \training\akosma\math\Fraction
     */
    public function __clone() {
        return Fraction::create($this->getNumerator(),
            $this->getDenominator());
    }
}
<?php
declare(strict_types = 1);

/**
 * Created by PhpStorm.
 * User: akosma
 * Date: 06.08.17
 * Time: 16:56
 */

namespace training\akosma\math;

/**
 * Class Functions
 *
 * Groups some commonly used functions, so that they are autoloaded by PHP
 * together with all the other classes. At the time of this writing, PHP
 * does not autoload standalone functions, although there is an RFC
 * proposed for this:
 *
 * https://wiki.php.net/rfc/function_autoloading
 *
 * @package training\akosma\math
 */
final class Functions {
    /**
     * Returns the Greatest Common Divisor of two integers.
     * Recursive implementation.
     *
     * @param int $a
     * @param int $b
     *
     * @return int
     */
    public static function gcd(int $a, int $b): int {
        $amodb = $a % $b;
        if ($amodb !== 0) {
            return self::gcd($b, $amodb);
        }
        return $b;
    }
}

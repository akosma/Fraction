<?php
declare(strict_types = 1);

namespace Deployer;

require 'recipe/common.php';

// Configuration

set('repository', 'git@bitbucket.org:akosma/fraction.git');
set('git_tty', true); // [Optional] Allocate tty for git on first deployment
set('shared_files', []);
set('shared_dirs', []);
set('writable_dirs', []);
set('allow_anonymous_stats', false);
set('keep_releases', 5);
set('ssh_multiplexing', true);
set('main_url', 'akosma.org');
set('dir_path', '/home/akosmatr/www');

// Hosts

host('akosma.org')
    ->hostname('sl269.web.hostpoint.ch')
    ->user("akosmatr")
    ->stage('production')
    ->set('deploy_path', '{{dir_path}}/{{main_url}}');
    
// Tasks

desc('Deploy your project');
task('deploy', [
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:writable',
    'deploy:vendors',
    'deploy:clear_paths',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
    'success'
]);

desc('Flushes the PHP OPC cache of the server');
task('flush_cache', function () {
    run('curl https://{{main_url}}/flush_cache.php');
});

desc('Run tests in server');
task('test', function () {
    cd('{{release_path}}');
    run('/usr/local/php71/bin/php {{dir_path}}/phpunit.phar', ['timeout' => null, 'tty' => true]);
});

after('deploy', 'flush_cache');
after('rollback', 'flush_cache');

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');
